<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appDevDebugProjectContainerUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevDebugProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_wdt')), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if (rtrim($pathinfo, '/') === '/_profiler') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_profiler_home');
                    }

                    return array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                }

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ($pathinfo === '/_profiler/search') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ($pathinfo === '/_profiler/search_bar') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_info
                if (0 === strpos($pathinfo, '/_profiler/info') && preg_match('#^/_profiler/info/(?P<about>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_info')), array (  '_controller' => 'web_profiler.controller.profiler:infoAction',));
                }

                // _profiler_phpinfo
                if ($pathinfo === '/_profiler/phpinfo') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_search_results')), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler')), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_router')), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception')), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception_css')), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

            // _twig_error_test
            if (0 === strpos($pathinfo, '/_error') && preg_match('#^/_error/(?P<code>\\d+)(?:\\.(?P<_format>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_twig_error_test')), array (  '_controller' => 'twig.controller.preview_error:previewErrorPageAction',  '_format' => 'html',));
            }

        }

        // homepage
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'homepage');
            }

            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::indexAction',  '_route' => 'homepage',);
        }

        if (0 === strpos($pathinfo, '/jours')) {
            // app_jour_getjour
            if (preg_match('#^/jours/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_app_jour_getjour;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'app_jour_getjour')), array (  '_controller' => 'AppBundle\\Controller\\JourController::getJourAction',));
            }
            not_app_jour_getjour:

            // app_jour_getjours
            if ($pathinfo === '/jours') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_app_jour_getjours;
                }

                return array (  '_controller' => 'AppBundle\\Controller\\JourController::getJoursAction',  '_route' => 'app_jour_getjours',);
            }
            not_app_jour_getjours:

            // app_jour_postjours
            if ($pathinfo === '/jours') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_app_jour_postjours;
                }

                return array (  '_controller' => 'AppBundle\\Controller\\JourController::postJoursAction',  '_route' => 'app_jour_postjours',);
            }
            not_app_jour_postjours:

            // app_jour_removejour
            if (preg_match('#^/jours/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'DELETE') {
                    $allow[] = 'DELETE';
                    goto not_app_jour_removejour;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'app_jour_removejour')), array (  '_controller' => 'AppBundle\\Controller\\JourController::removeJourAction',));
            }
            not_app_jour_removejour:

            // app_jour_updatejour
            if (preg_match('#^/jours/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'PUT') {
                    $allow[] = 'PUT';
                    goto not_app_jour_updatejour;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'app_jour_updatejour')), array (  '_controller' => 'AppBundle\\Controller\\JourController::updateJourAction',));
            }
            not_app_jour_updatejour:

        }

        if (0 === strpos($pathinfo, '/reservations')) {
            // app_reservation_getreservation
            if (preg_match('#^/reservations/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_app_reservation_getreservation;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'app_reservation_getreservation')), array (  '_controller' => 'AppBundle\\Controller\\ReservationController::getReservationAction',));
            }
            not_app_reservation_getreservation:

            // app_reservation_getreservations
            if ($pathinfo === '/reservations') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_app_reservation_getreservations;
                }

                return array (  '_controller' => 'AppBundle\\Controller\\ReservationController::getReservationsAction',  '_route' => 'app_reservation_getreservations',);
            }
            not_app_reservation_getreservations:

            // app_reservation_postreservations
            if ($pathinfo === '/reservations') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_app_reservation_postreservations;
                }

                return array (  '_controller' => 'AppBundle\\Controller\\ReservationController::postReservationsAction',  '_route' => 'app_reservation_postreservations',);
            }
            not_app_reservation_postreservations:

            // app_reservation_removereservation
            if (preg_match('#^/reservations/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'DELETE') {
                    $allow[] = 'DELETE';
                    goto not_app_reservation_removereservation;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'app_reservation_removereservation')), array (  '_controller' => 'AppBundle\\Controller\\ReservationController::removeReservationAction',));
            }
            not_app_reservation_removereservation:

            // app_reservation_updatereservation
            if (preg_match('#^/reservations/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'PATCH') {
                    $allow[] = 'PATCH';
                    goto not_app_reservation_updatereservation;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'app_reservation_updatereservation')), array (  '_controller' => 'AppBundle\\Controller\\ReservationController::updateReservationAction',));
            }
            not_app_reservation_updatereservation:

        }

        if (0 === strpos($pathinfo, '/vehicules')) {
            // app_vehicule_getvehicule
            if (preg_match('#^/vehicules/(?P<token>[^/]++)/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_app_vehicule_getvehicule;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'app_vehicule_getvehicule')), array (  '_controller' => 'AppBundle\\Controller\\VehiculeController::getVehiculeAction',));
            }
            not_app_vehicule_getvehicule:

            // app_vehicule_getvehicules
            if (preg_match('#^/vehicules/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_app_vehicule_getvehicules;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'app_vehicule_getvehicules')), array (  '_controller' => 'AppBundle\\Controller\\VehiculeController::getVehiculesAction',));
            }
            not_app_vehicule_getvehicules:

            // app_vehicule_getvehiculesdispo
            if (rtrim($pathinfo, '/') === '/vehicules/dispo') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_app_vehicule_getvehiculesdispo;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'app_vehicule_getvehiculesdispo');
                }

                return array (  '_controller' => 'AppBundle\\Controller\\VehiculeController::getVehiculesDispoAction',  '_route' => 'app_vehicule_getvehiculesdispo',);
            }
            not_app_vehicule_getvehiculesdispo:

            // app_vehicule_postvehicules
            if ($pathinfo === '/vehicules') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_app_vehicule_postvehicules;
                }

                return array (  '_controller' => 'AppBundle\\Controller\\VehiculeController::postVehiculesAction',  '_route' => 'app_vehicule_postvehicules',);
            }
            not_app_vehicule_postvehicules:

        }

        // app_vehicule_removevehicule
        if (0 === strpos($pathinfo, '/reservations') && preg_match('#^/reservations/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'DELETE') {
                $allow[] = 'DELETE';
                goto not_app_vehicule_removevehicule;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'app_vehicule_removevehicule')), array (  '_controller' => 'AppBundle\\Controller\\VehiculeController::removeVehiculeAction',));
        }
        not_app_vehicule_removevehicule:

        // app_vehicule_updatevehicule
        if (0 === strpos($pathinfo, '/vehicules') && preg_match('#^/vehicules/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'PUT') {
                $allow[] = 'PUT';
                goto not_app_vehicule_updatevehicule;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'app_vehicule_updatevehicule')), array (  '_controller' => 'AppBundle\\Controller\\VehiculeController::updateVehiculeAction',));
        }
        not_app_vehicule_updatevehicule:

        if (0 === strpos($pathinfo, '/jours')) {
            // remove_jour
            if (preg_match('#^/jours/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'DELETE') {
                    $allow[] = 'DELETE';
                    goto not_remove_jour;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'remove_jour')), array (  '_controller' => 'AppBundle\\Controller\\JourController::removeJourAction',  '_format' => NULL,));
            }
            not_remove_jour:

            // update_jour
            if (preg_match('#^/jours/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'PUT') {
                    $allow[] = 'PUT';
                    goto not_update_jour;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'update_jour')), array (  '_controller' => 'AppBundle\\Controller\\JourController::updateJourAction',  '_format' => NULL,));
            }
            not_update_jour:

            // get_jour
            if (preg_match('#^/jours/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_get_jour;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'get_jour')), array (  '_controller' => 'AppBundle\\Controller\\JourController::getJourAction',  '_format' => NULL,));
            }
            not_get_jour:

            // get_jours
            if ($pathinfo === '/jours') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_get_jours;
                }

                return array (  '_controller' => 'AppBundle\\Controller\\JourController::getJoursAction',  '_format' => NULL,  '_route' => 'get_jours',);
            }
            not_get_jours:

            // post_jours
            if ($pathinfo === '/jours') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_post_jours;
                }

                return array (  '_controller' => 'AppBundle\\Controller\\JourController::postJoursAction',  '_format' => NULL,  '_route' => 'post_jours',);
            }
            not_post_jours:

        }

        // remove_vehicule
        if (0 === strpos($pathinfo, '/reservations') && preg_match('#^/reservations/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'DELETE') {
                $allow[] = 'DELETE';
                goto not_remove_vehicule;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'remove_vehicule')), array (  '_controller' => 'AppBundle\\Controller\\VehiculeController::removeVehiculeAction',  '_format' => NULL,));
        }
        not_remove_vehicule:

        if (0 === strpos($pathinfo, '/vehicules')) {
            // update_vehicule
            if (preg_match('#^/vehicules/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'PUT') {
                    $allow[] = 'PUT';
                    goto not_update_vehicule;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'update_vehicule')), array (  '_controller' => 'AppBundle\\Controller\\VehiculeController::updateVehiculeAction',  '_format' => NULL,));
            }
            not_update_vehicule:

            // get_vehicule
            if (preg_match('#^/vehicules/(?P<token>[^/]++)/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_get_vehicule;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'get_vehicule')), array (  '_controller' => 'AppBundle\\Controller\\VehiculeController::getVehiculeAction',  '_format' => NULL,));
            }
            not_get_vehicule:

            // get_vehicules
            if (preg_match('#^/vehicules/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_get_vehicules;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'get_vehicules')), array (  '_controller' => 'AppBundle\\Controller\\VehiculeController::getVehiculesAction',  '_format' => NULL,));
            }
            not_get_vehicules:

            // get_vehicules_dispo
            if (rtrim($pathinfo, '/') === '/vehicules/dispo') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_get_vehicules_dispo;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'get_vehicules_dispo');
                }

                return array (  '_controller' => 'AppBundle\\Controller\\VehiculeController::getVehiculesDispoAction',  '_format' => NULL,  '_route' => 'get_vehicules_dispo',);
            }
            not_get_vehicules_dispo:

            // post_vehicules
            if ($pathinfo === '/vehicules') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_post_vehicules;
                }

                return array (  '_controller' => 'AppBundle\\Controller\\VehiculeController::postVehiculesAction',  '_format' => NULL,  '_route' => 'post_vehicules',);
            }
            not_post_vehicules:

        }

        if (0 === strpos($pathinfo, '/reservations')) {
            // remove_reservation
            if (preg_match('#^/reservations/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'DELETE') {
                    $allow[] = 'DELETE';
                    goto not_remove_reservation;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'remove_reservation')), array (  '_controller' => 'AppBundle\\Controller\\ReservationController::removeReservationAction',  '_format' => NULL,));
            }
            not_remove_reservation:

            // update_reservation
            if (preg_match('#^/reservations/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if ($this->context->getMethod() != 'PATCH') {
                    $allow[] = 'PATCH';
                    goto not_update_reservation;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'update_reservation')), array (  '_controller' => 'AppBundle\\Controller\\ReservationController::updateReservationAction',  '_format' => NULL,));
            }
            not_update_reservation:

            // get_reservation
            if (preg_match('#^/reservations/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_get_reservation;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'get_reservation')), array (  '_controller' => 'AppBundle\\Controller\\ReservationController::getReservationAction',  '_format' => NULL,));
            }
            not_get_reservation:

            // get_reservations
            if ($pathinfo === '/reservations') {
                if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'HEAD'));
                    goto not_get_reservations;
                }

                return array (  '_controller' => 'AppBundle\\Controller\\ReservationController::getReservationsAction',  '_format' => NULL,  '_route' => 'get_reservations',);
            }
            not_get_reservations:

            // post_reservations
            if ($pathinfo === '/reservations') {
                if ($this->context->getMethod() != 'POST') {
                    $allow[] = 'POST';
                    goto not_post_reservations;
                }

                return array (  '_controller' => 'AppBundle\\Controller\\ReservationController::postReservationsAction',  '_format' => NULL,  '_route' => 'post_reservations',);
            }
            not_post_reservations:

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
