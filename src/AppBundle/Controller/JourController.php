<?php
namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use AppBundle\Entity\Jour;
use Unirest;
class JourController extends Controller
{
    public function auth($token)
    {
             $url="http://chezmeme.com/sso/api/v1.5/check_access/".$token."/vehicules"; 
                    $body = Unirest\Request\Body::json($query);
                    $headers = array('Accept' => 'application/json');
                    $response = Unirest\Request::get($url,$headers,$body);
                  
                            if ($response->raw_body=="{\"ret\":\"denied\"}")
                            {
                                return "AU002";
                            }
                            if ($response->raw_body=="{\"ret\":\"expired\"}")
                            {
                                return "AU003";
                            }
                            return "AU";
    }
/**
     * @Rest\View()
     * @Rest\Get("/jours/{id}")
     */
    public function getJourAction(Request $request)
    {
        
        $jour = $this->get('doctrine.orm.entity_manager')
                ->getRepository('AppBundle:Jour')
                 ->find($request->get('id'));
        /* @var $jour Jour */
         if (empty($jour)) {
            return new JsonResponse(['message' => 'Jour not found'], Response::HTTP_NOT_FOUND);
        }
       

        return $jour;
    }
/**
     * @Rest\View()
     * @Rest\Get("/jours/{token}/")
     */
    public function getJoursAction(Request $request)
    {
        $auth=$this->auth($request->get('token'));
        if ($auth=="AU002"||$auth=="AU003"){
            return $auth;
        }    
        $jours = $this->get('doctrine.orm.entity_manager')
                ->getRepository('AppBundle:Jour')
                ->findAll();
        /* @var $jours Jour[] */
        $response=new Response();
        $response->setContent(json_encode($jours));
        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');
        //$response->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, PATCH, OPTIONS');
        return $response;
    }
     /**
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     * @Rest\Post("/jours")
     */
    public function postJoursAction(Request $request)
    {
        $jour = new Jour($request->get('nom'),$request->get('dispo'));
        $form = $this->createForm(JourType::class, $jour);

        $form->submit($request->request->all()); // Validation des données

        if ($form->isValid()) {
            $em = $this->get('doctrine.orm.entity_manager');
            $em->persist($jour);
            $em->flush();
            return $jour;
        } else {
            return $form;
        }
    }
         /**
     * @Rest\View(statusCode=Response::HTTP_NO_CONTENT)
     * @Rest\Delete("/jours/{id}")
     */
    public function removeJourAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $jour = $em->getRepository('AppBundle:Jour')
                    ->find($request->get('id'));
        /* @var $jour Jour */

        if ($jour) {
            $em->remove($joue);
            $em->flush();
        }
    }
    
     /**
     * @Rest\View()
     * @Rest\Put("/jours/{id}")
     */
    public function updateJourAction(Request $request)
    {
        $jour = $this->get('doctrine.orm.entity_manager')
                ->getRepository('AppBundle:Jour')
                ->find($request->get('id'));
        /* @var $jour Jour */

        if (empty($jour)) {
            return new JsonResponse(['message' => 'Jour not found'], Response::HTTP_NOT_FOUND);
        }

        $form = $this->createForm(JourType::class, $jour);

        $form->submit($request->request->all());

        if ($form->isValid()) {
            $em = $this->get('doctrine.orm.entity_manager');
            $em->merge($jour);
            $em->flush();
            return $jour;
        } else {
            return $form;
        }
    }
}
