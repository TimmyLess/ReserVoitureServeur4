<?php
namespace AppBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest; 
use AppBundle\Entity\Vehicule;
use AppBundle\Entity\Reservation;
use Unirest;
class VehiculeController extends Controller
{ 
    public function auth($token)
    {
             $url="http://chezmeme.com/sso/api/v1.5/check_access/".$token."/vehicules"; 
                    $body = Unirest\Request\Body::json($query);
                    $headers = array('Accept' => 'application/json');
                    $response = Unirest\Request::get($url,$headers,$body);
                  
                            if ($response->raw_body=="{\"ret\":\"denied\"}")
                            {
                                return "AU002";
                            }
                            if ($response->raw_body=="{\"ret\":\"expired\"}")
                            {
                                return "AU003";
                            }
                            return "AU";
    }
    /**
     * @Rest\View()
     * @Rest\Get("/vehicules/{token}/{id}")
     */
    //on doit récupérer la liste des reservations correspondante à l'identifiant du vehicule
    public function getVehiculeAction(Request $request)
    {
        
        $auth=$this->auth($request->get('token'));
        if ($auth=="AU002"||$auth=="AU003"){
            return $auth;
        } 
         /* @var $vehicule Vehicule */

         $vehicule = $this->get('doctrine.orm.entity_manager')
                ->getRepository('AppBundle:Vehicule')
                 ->findAll();
        $vt=true;
   
        foreach ($vehicule as $v)
        {
            if (strcmp($v->immatriculation,$request->get('id'))==0)
            {
                $vt=false;
            }
        }
         if ($vt) {
           return "DT003";
        }
       
        
        
          $maDate=$_GET['date'];
           if (strcmp(date('D', strtotime($_GET['date'])),"Mon")==0) {
               
                $leLundi=date("Y-m-d", strtotime($maDate)); 
           }
        else
        {
            $leLundi=date("Y-m-d", strtotime("last monday", strtotime($maDate))); 
        }
          
          $reservations = $this->get('doctrine.orm.entity_manager')
                    ->getRepository('AppBundle:Reservation')
                    ->findAll();
            /* @var $reservations Reservation[] */
           $planning=array();
           $reservationVehicules=array();
           
           foreach ($reservations as $reservation)
           {
               // on filtre les reservations pour le vehicule ad hoc
               if ($reservation->getVehicule()==$request->get('id'))
               {
                  array_push($reservationVehicules,$reservation);
               }
           }
                   for ($i=0;$i<7;$i++) // chaque jour
                   {
                       $data=array();
                       $date = date("Y-m-d", strtotime("+".$i." day", strtotime($leLundi))); 
                       for ($d=0;$d<2;$d++) // chaque demi journée 
                        {
                           
                              foreach ($reservationVehicules as $reservationVehicule)
                            {
                                $dateBis = $reservationVehicule->getDateDebut()->format('Y-m-d H:i:s');
                                if (strcmp(date('A', strtotime($dateBis)),"AM")==0)
                                {
                                    $RdemiJ=0; // c'est le matin
                                }
                                else
                                {
                                    $RdemiJ=1; // c'est pas le matin
                                }
                                  
                                $dateBis = $reservationVehicule->getDateDebut()->format('Y-m-d');
                                $dateJour=$date;
                
                                 if (strcmp($dateJour,$dateBis)==0)
                                {
                                    if (!isset($nbDJ)||true) 
                                    {
                                        if ($RdemiJ==$d)
                                        {
                                           
                                            $nbDJ=$reservationVehicule->nbDemiJournee;
                                            if ($reservationVehicule->getKmDepart()==0&&$reservationVehicule->getKmArrivee()==0)
                                            {
                                                $etat="reserve";
                                                $etatsave="reserve";
                                            }
                                            else
                                            {
                                                $etat="retour";
                                                $etatsave="retour";
                                            }
                                            $data[0]= $reservationVehicule;
                                            $id2=$reservationVehicule->getId();
                                            $data["id"]=$reservationVehicule->getId();
                                            $data2=$reservationVehicule;
                                        }
                                    }
                                }else {unset($etat);}
                                 
                              }
                                if (isset($nbDJ))
                                    {
                                        if ($nbDJ==0)
                                        {
                                            unset($data);
                                            unset($nbDJ);
                                            unset($data2);
                                            unset($etatsave);
                                        }
                                        else
                                        {
                                            $data[0]=$data2;
                                            $data["id"]=$id2;
                                            $etat=$etatsave;
                                            $nbDJ--;
                                        }
                                        
                                    } 
                            if (!isset($etat))
                            {
                                $etat="dispo";
                            }
                           if ($data==null)
                           {
                               $data=array();
                               $etat="dispo";
                           }
                     $val=array("date"=>$date,"demiJ"=>$d,"etat"=>$etat,"data"=>$data);
                     array_push($planning, $val);    
                    }
                   }
         if (empty($planning)) {
            return new JsonResponse([], Response::HTTP_NOT_FOUND);
        }
        $response=new Response();
        $response->setContent(json_encode($planning));
        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');
        //$response->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, PATCH, OPTIONS');
        return $response;
    }
  /**
     * @Rest\View()
     * @Rest\Get("/vehicules/{token}")
     */
    public function getVehiculesAction(Request $request)
    {
        
        
        $auth=$this->auth($request->get('token'));
        if ($auth=="AU002"||$auth=="AU003"){
            return $auth;
        }           
                    
        
        $vehicules = $this->get('doctrine.orm.entity_manager')
                ->getRepository('AppBundle:Vehicule')
                ->findAll();
        /* @var $vehicules Vehicule[] */
        $result=array();
        $i=0;
       
        
        foreach ($vehicules as $vehicule)
        {
           $result[$i]=$vehicule->getOneVehicule();
           $i++;
        }
         $response=new Response();
        $response->setContent(json_encode($result));
        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');
        //$response->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, PATCH, OPTIONS');
        return $response;
    }
    /**
     * @Rest\View()
     * @Rest\Get("/vehicules/dispo/{token}/")
     */
    public function getVehiculesDispoAction(Request $request)
    {
        
        $auth=$this->auth($request->get('token'));
        if ($auth=="AU002"||$auth=="AU003"){
            return $auth;
        }       
         $vehicules = $this->get('doctrine.orm.entity_manager')
                ->getRepository('AppBundle:Vehicule')
                ->findAll();
        $result=$vehicules;
        $resultbis= array();
        /* @var $vehicules Vehicule[] */
        $debut=$request->get('dateDebut');
        $fin=$request->get('dateFin');
        $places=$request->get('places');
        if ($places==2||$places==4)
        {
           $continuer=true;
        }
        else
        {
            return "DT002";
        }
        
        $date_debut_1 = new \DateTime($debut);
        $date_fin_1 = new \DateTime($fin);
        if ($date_debut_1 >  $date_fin_1)
        {
            return "DT001";
        }
        $reservations = $this->get('doctrine.orm.entity_manager')
                    ->getRepository('AppBundle:Reservation')
                    ->findAll();
            /* @var $reservations Reservation[] */
        //on enlève du résultat les véhicules qui n'on pas le nombre de place requis
        foreach($result as $i=>$vehicule)
                  {
            
                      if ($vehicule->getPlaces()<$places)
                      {
                          unset($result[$i]);
                      }else{
                          $result[$i]=$vehicule->getOneVehicule2();
                          
                      }
                  }
        
        foreach ($reservations as $reservation)
        {
            $date_debut_2= new \DateTime($reservation->getDateDebut()->format('Y-m-d H:i:s'));
            $Htot=12*$reservation->getnbDemiJournee();
            $date_fin_2= new \DateTime($reservation->getDateDebut()->add(new \DateInterval('PT'.$Htot.'H'))->format('Y-m-d H:i:s'));
            
            if  ( ( ($date_debut_2 <=  $date_fin_1) && ($date_debut_2 >= $date_debut_1) ) || ( ($date_fin_1 <=  $date_fin_2) && ($date_fin_1 >=  $date_debut_2) ) )
            {
                             foreach($result as $j=>$vehicule)
                          {
                              if (strcmp($vehicule["immmatriculation"],$reservation->getVehicule())==0)
                              {
                                  unset($result[$j]); // on supprime le vehicule associé au s  
                              }
                          }
            } 
            
        }
        foreach ($result as $row)
        {
            array_push($resultbis,$row);
        }
        $response=new Response();
        $response->setContent(json_encode($resultbis));
        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');
        //$response->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, PATCH, OPTIONS');
        
        return $response;
    }
     /**
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     * @Rest\Post("/vehicules")
     */
    public function postVehiculesAction(Request $request)
    {
        $vehicule = new Vehicule();
        $form = $this->createForm(VehiculeType::class, $vehicule);

        $form->submit($request->request->all());

        if ($form->isValid()) {
            $em = $this->get('doctrine.orm.entity_manager');
            $em->persist($user);
            $em->flush();
            return $vehicule;
        } else {
            return $form;
        }
    }
            /**
     * @Rest\View(statusCode=Response::HTTP_NO_CONTENT)
     * @Rest\Delete("/reservations/{id}")
     */
     public function removeVehiculeAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $vehicule = $em->getRepository('AppBundle:Vehicule')
                    ->find($request->get('id'));
        /* @var $vehicule Vehicule */

        if ($vehicule) {
            $em->remove($vehicule);
            $em->flush();
        }
    }
     /**
     * @Rest\View()
     * @Rest\Put("/vehicules/{id}")
     */
    public function updateVehiculeAction(Request $request)
    {
        $vehicule = $this->get('doctrine.orm.entity_manager')
                ->getRepository('AppBundle:Vehicule')
                ->find($request->get('id')); // L'identifiant en tant que paramètre n'est plus nécessaire
        /* @var $vehicule Vehicule */

        if (empty($vehicule)) {
            return new JsonResponse(['message' => 'Vehicule not found'], Response::HTTP_NOT_FOUND);
        }

        $form = $this->createForm(VehiculeType::class, $vehicule);

        $form->submit($request->request->all());

        if ($form->isValid()) {
            $em = $this->get('doctrine.orm.entity_manager');
            // l'entité vient de la base, donc le merge n'est pas nécessaire.
            // il est utilisé juste par soucis de clarté
            $em->merge($vehicule);
            $em->flush();
            return $vehicule;
        } else {
            return $form;
        }
    }
}

