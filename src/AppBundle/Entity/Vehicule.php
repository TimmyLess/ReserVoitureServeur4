<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="Vehicule",
*      uniqueConstraints={@ORM\UniqueConstraint(name="vehicules_immatriculation_unique",columns={"immatriculation"})}
* )
 */

class Vehicule
{
/**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;
    /**
     * @ORM\Column(type="string")
     */
	public $marque;
    /**
     * @ORM\Column(type="string")
     */
	public $modele;
    /**
     * @ORM\Column(type="string")
     */
	public $couleur;
    /**
     * @ORM\Column(type="string")
     */
	public $immatriculation;
    /**
     * @ORM\Column(type="integer")
     */
	public $places;
  
	

    public function __construct($marque,$modele,$couleur,$immatriculation,$places)
    {
        $this->places = $places;
        $this->marque= $marque;
        $this->couleur = $couleur;
        $this->immatriculation= $immatriculation;
        $this->modele = $modele;
	
    }
	public function getMarque()
    {
        return $this->marque;
    }
    public function setMarque($marque){
        $this->marque=$marque;
        return $this;
    }
    public function getOneVehicule()
    {
        $ret = array();
        $ret["marque"] = $this->marque;
        $ret["couleur"] = $this->couleur;
        $ret["immmatriculation"] = $this->immatriculation;
        $ret["modele"] = $this->modele;
        
        return $ret;
    }
       public function getOneVehicule2()
    {
        $ret = array();
        $ret["marque"] = $this->marque;
        $ret["couleur"] = $this->couleur;
        $ret["immmatriculation"] = $this->immatriculation;
        $ret["modele"] = $this->modele;
        $ret["places"] = $this->places;
        return $ret;
    }
    public function getModele()
    {
        return $this->modele;
    }
    public function setModele($modele)
    {
        $this->modele=$modele;
        return $this;
    }
    public function getCouleur()
    {
        return $this->couleur;
    }
    public function setCouleur($couleur)
    {
        $this->couleur=$couleur;
        return $this;
    }
    public function getImmatriculation()
    {
        return $this->immatriculation;
    }
    public function setImmatriculation($immatriculation)
    {
        $this->immatriculation=$immatriculation;
        return $this;
        
    }
   
    public function getPlaces()
    {
        return $this->places;
    }
    public function setPlaces($places)
    {
        $this->places=$places;
        return $this;
    }
}