ReserVoitureServeur2
====================

git clone https://gitlab.com/TimmyLess/ReserVoitureServeur2.git
cd ReserVoitureServeur2
composer install
On édite les paramètres relatifs à la bdd :
    # app/config/parameters.yml
    parameters:
        database_host: 127.0.0.1
        database_port: null
        database_name: rest_api
        database_user: root
        database_password: null
        
php bin/console doctrine:database:create
php bin/console doctrine:schema:update --dump-sql --force

A Symfony project created on January 17, 2018, 11:29 am.
