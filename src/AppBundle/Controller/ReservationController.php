<?php
namespace AppBundle\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use AppBundle\Entity\Reservation;
use AppBundle\Form\Type\ReservationType;
use Unirest;
class ReservationController extends Controller
{
      public function auth($token)
    {
             $url="http://chezmeme.com/sso/api/v1.5/check_access/".$token."/vehicules"; 
                    $body = Unirest\Request\Body::json($query);
                    $headers = array('Accept' => 'application/json');
                    $response = Unirest\Request::get($url,$headers,$body);
                  
                            if ($response->raw_body=="{\"ret\":\"denied\"}")
                            {
                                return "AU002";
                            }
                            if ($response->raw_body=="{\"ret\":\"expired\"}")
                            {
                                return "AU003";
                            }
                            return "AU";
    }
        /**
     * @Rest\View()
     * @Rest\Get("/reservations/{id}")
     */
    public function getReservationAction(Request $request)
    {
          
        
        /* @var $reservation Reservation */

        $reservation = $this->get('doctrine.orm.entity_manager')
                ->getRepository('AppBundle:Reservation')
                 ->find($request->get('id'));

         if (empty($reservation)) {
            return new JsonResponse(['message' => 'Reservation not found'], Response::HTTP_NOT_FOUND);
        }
       

        return $reservation;
    }
/**
     * @Rest\View()
     * @Rest\Get("/reservations")
     */
    public function getReservationsAction(Request $request)
    {
            $reservations = $this->get('doctrine.orm.entity_manager')
                    ->getRepository('AppBundle:Reservation')
                    ->findAll();
            /* @var $reservations Reservation[] */
            return $reservations;
    }
     /**
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     * @Rest\Post("/reservations/{token}")
     */ 
    public function postReservationsAction(Request $request)
    {
        $auth=$this->auth($request->get('token'));
        if ($auth=="AU002"||$auth=="AU003"){
            return $auth;
        }    
         $url="http://chezmeme.com/sso/api/v1.5/who_is/".$request->get('token'); 
                    $body = Unirest\Request\Body::json($query);
                    $headers = array('Accept' => 'application/json');
                    $response = Unirest\Request::get($url,$headers,$body);
                    $try=file_get_contents($url);
       
        $date=new \DateTime($request->get('dateDebut'));
        $reservation = new Reservation($date,$request->get('nbDemiJournees'),$request->get('vehicule'),$request->get('conducteur'),json_decode($try,true)["id"]);
       
        $lreservations = $this->get('doctrine.orm.entity_manager')
                    ->getRepository('AppBundle:Reservation')
                    ->findAll();
            /* @var $lreservations Reservation[] */
        
        $ndjh=$request->get('nbDemiJournees')*12;
        $date_debut_1=new \DateTime($request->get('dateDebut'));
        $date_fin_1=new \DateTime($request->get('dateDebut'));
        $date_fin_1->add(new \DateInterval('PT'.$ndjh.'H'));
 
        foreach ($lreservations as $r)
        {
            $date_debut_2=$r->getDateDebut();
            $ndjh2=$r->getnbDemiJournee()*12;
            $date_fin_2=$r->getDateDebut();
            $date_fin_2->add(new \DateInterval('PT'.$ndjh2.'H'));
            if  (( ($date_debut_2 <  $date_fin_1) && ($date_debut_2 > $date_debut_1) ) || ( ($date_fin_1 <  $date_fin_2) && ($date_fin_1 >  $date_debut_2) ))
            {
                return "DT004";
            }
            
        }
        
        
        
        /*
        $form = $this->createForm(ReservationType::class, $reservation);
        $form->submit($request->request->all());
        
        if ($form->isValid()) {
        */
            $em = $this->get('doctrine.orm.entity_manager');
            $em->persist($reservation);
            $em->flush();
         $response=new Response();
        $response->setContent(json_encode($reservation->getId()));
        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');
        //$response->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, PATCH, OPTIONS');
        return $response;
            
        /*
        } else {
            return $form;
        }
        /*
         return [
            'Reservation' => [
                "date"=>$request->get('dateDebut'),
                "nbDemiJournee"=>$request->get('nbDemiJournee'),
                "vehicule"=> $request->get('vehicule'),
                "user"=>$request->get('user'),
                "conducteur"=>$request->get('conducteur')
                
             ]
         ];*/
    }
         /**
     * @Rest\View(statusCode=Response::HTTP_NO_CONTENT)
     * @Rest\Delete("/reservations/{id}")
     */
    public function removeReservationAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $reservation = $em->getRepository('AppBundle:Reservation')
                    ->find($request->get('id'));
        /* @var $reservation Reservation */

        if ($reservation) {
            $em->remove($reservation);
            $em->flush();
        }
    }
    
    /**
     * @Rest\View()
     * @Rest\Patch("/reservations/{token}/{id}")
     */
    public function updateReservationAction(Request $request)
    {
                $auth=$this->auth($request->get('token'));
        if ($auth=="AU002"||$auth=="AU003"){
            return $auth;
        }    
        
                $reservation = $this->get('doctrine.orm.entity_manager')
                        ->getRepository('AppBundle:Reservation')
                        ->find($request->get('id')); // L'identifiant en tant que paramètre n'est plus nécessaire
                /* @var $reservation Reservation */

                if (empty($reservation)) {
                    return new JsonResponse(['message' => 'Reservation not found'], Response::HTTP_NOT_FOUND);
                }

                if ($request->get('dateDebut')!=null) {
                    $reservation->setDateDebut(new \DateTime($request->get('dateDebut')));
                }
        
        
                if ($request->get('vehicule')!=null) {
                    $reservation->setVehicule($request->get('vehicule'));
                }
                if ($request->get('kmDepart')>$request->get('km'))
                {
                    return "DT007";
                }
        
                if ($request->get('user')!=null)$reservation->setUser($request->get('user'));
                
                if ($request->get('kmDepart')!=null)$reservation->setkmDepart($request->get('kmDepart'));
                if ($request->get('km')!=null)$reservation->setKmArrivee($request->get('km'));
                if ($request->get('conducteur')!=null)$reservation->setConducteur($request->get('conducteur'));
                if ($request->get('pb')!=null)$reservation->setPbTechnique ($request->get('pb'));
                if ($request->get('nbDemiJournee')!=null)$reservation->setnbDemiJournee($request->get('nbDemiJournee'));
                $em = $this->get('doctrine.orm.entity_manager');
                $em->merge($reservation);
                $em->flush();
                $response=new Response();
                $response->setContent(json_encode($reservation->getId()));
                $response->headers->set('Content-Type', 'application/json');
                $response->headers->set('Access-Control-Allow-Origin', '*');
                $response->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, PATCH, OPTIONS');
                return $response;
                
                
            
            }
    
   
}
