<?php
namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity()
 * @ORM\Table(name="Reservation")
 */
class Reservation
{
/**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;
     /**
     * @ORM\Column(type="datetime")
     */
	public $dateDebut;
         /**
     * @ORM\Column(type="integer")
     */
	public $nbDemiJournee;
         /**
     * @ORM\Column(type="string")
     */
	public $vehicule;
         /**
     * @ORM\Column(type="integer")
     */
	public $user;
         /**
     * @ORM\Column(type="integer")
     */
	public $kmDepart;
             /**
     * @ORM\Column(type="integer")
     */
	public $kmArrivee;
             /**
     * @ORM\Column(type="string")
     */
	public $pbTechnique;
	          /**
     * @ORM\Column(type="integer")
     */
	public $conducteur;
    public function __construct($debut,$demiJournee,$vehicule,$conducteur,$user)
    {
        
        $this->dateDebut= $debut;
        $this->nbDemiJournee= $demiJournee;
        $this->vehicule= $vehicule;
        $this->user= $user;
        $this->kmDepart=0;
        $this->kmArrivee=0;
        $this->pbTechnique=" ";
        $this->conducteur=$conducteur;
    }
      public function getId()
    {
        return $this->id;
    }
    public function getDateDebut()
    {
        return $this->dateDebut;
    }
    public function setDateDebut($date)
    {
        $this->dateDebut=$date;
        return $this;
    }
        public function getnbDemiJournee()
    {
        return $this->nbDemiJournee;
    }
    public function setnbDemiJournee($nbDemiJournee)
    {
        $this->nbDemiJournee=$nbDemiJournee;
        return $this;
    }
    public function getVehicule()
    {
        return $this->vehicule;
    }
    
    public function setVehicule($vehicule)
    {
        $this->vehicule=$vehicule;
        return $this;
    }
    public function getUser()
    {
        return $this->user;
    }
    public function setUser($user)
    {
        $this->user=$user;
        return $this;
    }
    public function getKmDepart()
    {
        return $this->kmDepart;
    }
    public function setkmDepart($kmDepart)
    {
        $this->kmDepart=$kmDepart;
        return $this;
    }
    public function getKmArrivee()
    {
        return $this->kmArrivee;
    }
    public function setKmArrivee($km)
    {
        $this->kmArrivee=$km;
        return $this;
    }
        public function getConducteur()
    {
        return $this->conducteur;
    }
    public function setConducteur($conducteur)
    {
        $this->conducteur=$conducteur;
        return $this;
    }
    public function getPbTechnique()
    {
        return $this->pbTechnique;
    }
    public function setPbTechnique ($pb)
    {
        $this->pbTechnique=$pb;
        return $this;
    }
}