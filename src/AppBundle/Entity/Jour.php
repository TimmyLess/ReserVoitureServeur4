<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity()
 * @ORM\Table(name="Jour",
 *      uniqueConstraints={@ORM\UniqueConstraint(name="jour_nom_unique",columns={"nom"})})
 */
class Jour
{
/**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;
    /**
     * @ORM\Column(type="string")
     */
	public $nom;
    /**
     * @ORM\Column(type="boolean")
     */
	public $dispo;
	
    public function __construct($nom,$dispo)
    {
        $this->nom= $nom;
        $this->dispo= $dispo;
    }
    public function getNom()
    {
        return $this->nom;
    }
    
    public function setNom($nom)
    {
        $this->nom=$nom;
        return $this;
    }
    public function getDispo()
    {
        return $this->dispo;
    }
    public function setDispo($dispo)
    {
        $this->dispo=$dispo;
        return $this;
    }
}