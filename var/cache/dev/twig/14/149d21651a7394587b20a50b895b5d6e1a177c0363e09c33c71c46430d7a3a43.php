<?php

/* WebProfilerBundle:Profiler:ajax_layout.html.twig */
class __TwigTemplate_257f8af107cd3dd8b6250e7cc86ac14b28072a6d96f75e9e1a19c0fe43429989 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_61c4fb956bb872e699d59c5971d4d53fce62f8da367f8c65876549521393fb6b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_61c4fb956bb872e699d59c5971d4d53fce62f8da367f8c65876549521393fb6b->enter($__internal_61c4fb956bb872e699d59c5971d4d53fce62f8da367f8c65876549521393fb6b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajax_layout.html.twig"));

        // line 1
        $this->displayBlock('panel', $context, $blocks);
        
        $__internal_61c4fb956bb872e699d59c5971d4d53fce62f8da367f8c65876549521393fb6b->leave($__internal_61c4fb956bb872e699d59c5971d4d53fce62f8da367f8c65876549521393fb6b_prof);

    }

    public function block_panel($context, array $blocks = array())
    {
        $__internal_c942481b5a29aeb1959daa1aabbdaeafd068565e87386a862ff7cffa698d4fcb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c942481b5a29aeb1959daa1aabbdaeafd068565e87386a862ff7cffa698d4fcb->enter($__internal_c942481b5a29aeb1959daa1aabbdaeafd068565e87386a862ff7cffa698d4fcb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        echo "";
        
        $__internal_c942481b5a29aeb1959daa1aabbdaeafd068565e87386a862ff7cffa698d4fcb->leave($__internal_c942481b5a29aeb1959daa1aabbdaeafd068565e87386a862ff7cffa698d4fcb_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% block panel '' %}
", "WebProfilerBundle:Profiler:ajax_layout.html.twig", "/home/remy/Documents/ReserVoitureServeur2/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/ajax_layout.html.twig");
    }
}
