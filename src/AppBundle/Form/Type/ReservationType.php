<?php
namespace AppBundle\Form\Type;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReservationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('dateDebut');
        $builder->add('nbDemiJournee');
        $builder->add('vehicule');
        $builder->add('user');
        $builder->add('kmDepart');
        $builder->add('kmArrivee');
        $builder->add('pbTechnique');
        $builder->add('conducteur');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\Reservation',
            'csrf_protection' => false
        ]);
    }
}