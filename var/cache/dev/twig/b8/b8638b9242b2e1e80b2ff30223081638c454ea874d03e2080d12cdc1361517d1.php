<?php

/* WebProfilerBundle:Collector:router.html.twig */
class __TwigTemplate_0c06d56bb5ad927f860154cae9330bce134aa21fed6034780e74f5a2d5848db1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Collector:router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b25518d39f52016a9d688d21cdd35c314b8cc7671eb8c30a85e0cda71cad478b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b25518d39f52016a9d688d21cdd35c314b8cc7671eb8c30a85e0cda71cad478b->enter($__internal_b25518d39f52016a9d688d21cdd35c314b8cc7671eb8c30a85e0cda71cad478b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b25518d39f52016a9d688d21cdd35c314b8cc7671eb8c30a85e0cda71cad478b->leave($__internal_b25518d39f52016a9d688d21cdd35c314b8cc7671eb8c30a85e0cda71cad478b_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_a1244968f45fecc9bd596ec8bc0b98e9a9e9cb02586549970da87cd045e49def = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a1244968f45fecc9bd596ec8bc0b98e9a9e9cb02586549970da87cd045e49def->enter($__internal_a1244968f45fecc9bd596ec8bc0b98e9a9e9cb02586549970da87cd045e49def_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_a1244968f45fecc9bd596ec8bc0b98e9a9e9cb02586549970da87cd045e49def->leave($__internal_a1244968f45fecc9bd596ec8bc0b98e9a9e9cb02586549970da87cd045e49def_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_39eac641cd3c51ce4a19e7b803f10f26a44f3703f2d486a5614232ca901d1142 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_39eac641cd3c51ce4a19e7b803f10f26a44f3703f2d486a5614232ca901d1142->enter($__internal_39eac641cd3c51ce4a19e7b803f10f26a44f3703f2d486a5614232ca901d1142_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_39eac641cd3c51ce4a19e7b803f10f26a44f3703f2d486a5614232ca901d1142->leave($__internal_39eac641cd3c51ce4a19e7b803f10f26a44f3703f2d486a5614232ca901d1142_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_93055845f6c9adc0e64613158067040e9d48c66e5788d75829eea2af1758bd66 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_93055845f6c9adc0e64613158067040e9d48c66e5788d75829eea2af1758bd66->enter($__internal_93055845f6c9adc0e64613158067040e9d48c66e5788d75829eea2af1758bd66_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\HttpKernelExtension')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_93055845f6c9adc0e64613158067040e9d48c66e5788d75829eea2af1758bd66->leave($__internal_93055845f6c9adc0e64613158067040e9d48c66e5788d75829eea2af1758bd66_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "WebProfilerBundle:Collector:router.html.twig", "/home/remy/Documents/ReserVoitureServeur2/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/router.html.twig");
    }
}
