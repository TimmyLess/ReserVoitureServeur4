<?php

/* ::base.html.twig */
class __TwigTemplate_e2d21215726556d7a2b9206e83e6061cc251f39bee62896aecdbb35410598892 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ff27d9191fdd00d8a9ec404aae681116d0cbf0191dc9539526e77f31472b025d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ff27d9191fdd00d8a9ec404aae681116d0cbf0191dc9539526e77f31472b025d->enter($__internal_ff27d9191fdd00d8a9ec404aae681116d0cbf0191dc9539526e77f31472b025d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 10
        $this->displayBlock('body', $context, $blocks);
        // line 11
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 12
        echo "    </body>
</html>
";
        
        $__internal_ff27d9191fdd00d8a9ec404aae681116d0cbf0191dc9539526e77f31472b025d->leave($__internal_ff27d9191fdd00d8a9ec404aae681116d0cbf0191dc9539526e77f31472b025d_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_626b574697e2087eac4fb5f0d06157d48a805c42636a2fce29608877f5d7f4a5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_626b574697e2087eac4fb5f0d06157d48a805c42636a2fce29608877f5d7f4a5->enter($__internal_626b574697e2087eac4fb5f0d06157d48a805c42636a2fce29608877f5d7f4a5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_626b574697e2087eac4fb5f0d06157d48a805c42636a2fce29608877f5d7f4a5->leave($__internal_626b574697e2087eac4fb5f0d06157d48a805c42636a2fce29608877f5d7f4a5_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_306d9d25cdf32125250f4c45d82bd5861ac9c4e476da87e989f834a85a96f032 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_306d9d25cdf32125250f4c45d82bd5861ac9c4e476da87e989f834a85a96f032->enter($__internal_306d9d25cdf32125250f4c45d82bd5861ac9c4e476da87e989f834a85a96f032_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_306d9d25cdf32125250f4c45d82bd5861ac9c4e476da87e989f834a85a96f032->leave($__internal_306d9d25cdf32125250f4c45d82bd5861ac9c4e476da87e989f834a85a96f032_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_0b942bc2cda21aeb2e12b7cde9048f53656186ecfa824d3bdd371044003d797d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0b942bc2cda21aeb2e12b7cde9048f53656186ecfa824d3bdd371044003d797d->enter($__internal_0b942bc2cda21aeb2e12b7cde9048f53656186ecfa824d3bdd371044003d797d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_0b942bc2cda21aeb2e12b7cde9048f53656186ecfa824d3bdd371044003d797d->leave($__internal_0b942bc2cda21aeb2e12b7cde9048f53656186ecfa824d3bdd371044003d797d_prof);

    }

    // line 11
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_7aa3ce0a0f482da20049a14fe18cd14979a487f1ac3d214a619b9932eb0294b1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7aa3ce0a0f482da20049a14fe18cd14979a487f1ac3d214a619b9932eb0294b1->enter($__internal_7aa3ce0a0f482da20049a14fe18cd14979a487f1ac3d214a619b9932eb0294b1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_7aa3ce0a0f482da20049a14fe18cd14979a487f1ac3d214a619b9932eb0294b1->leave($__internal_7aa3ce0a0f482da20049a14fe18cd14979a487f1ac3d214a619b9932eb0294b1_prof);

    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 11,  82 => 10,  71 => 6,  59 => 5,  50 => 12,  47 => 11,  45 => 10,  38 => 7,  36 => 6,  32 => 5,  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Welcome!{% endblock %}</title>
        {% block stylesheets %}{% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
    </head>
    <body>
        {% block body %}{% endblock %}
        {% block javascripts %}{% endblock %}
    </body>
</html>
", "::base.html.twig", "/home/remy/Documents/ReserVoitureServeur2/app/Resources/views/base.html.twig");
    }
}
